package eu.ubitech.anastacia.IoTBrokerConnector.repository;

import eu.ubitech.anastacia.IoTBrokerConnector.model.Subscription;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;



import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface SubscriptionRepository extends MongoRepository<Subscription, String> {

    Optional<Subscription> findBySubscriptionId(String subscriptionId);
}
