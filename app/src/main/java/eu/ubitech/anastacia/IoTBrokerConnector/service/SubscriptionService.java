package eu.ubitech.anastacia.IoTBrokerConnector.service;


import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.AnySubscriptionIDException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidDurationException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidThrottlingException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @param <S> Subscription object
 */

@Service
public interface SubscriptionService<S, ID>  {
//    S findById(ID id);

    S register(S s) throws InvalidThrottlingException, InvalidDurationException, AnySubscriptionIDException;

    S setSubscriptionResponse(S s) throws InvalidThrottlingException, InvalidDurationException, AnySubscriptionIDException;

    S findBySubscriptionId(String subscriptionId);

//    List<S> findAll();

//    void updateSubscription(String username, S s);


//    void removeSubscription(String subscriptionId);

//    void disableSubscription(String subscriptionId);
}
