package eu.ubitech.anastacia.IoTBrokerConnector.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.SubscriptionDto;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.SubscriptionResponseDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public class IoTBrokerUtil {

    public static SubscriptionResponseDto sendSubscribe(String url, SubscriptionDto subscriptionDto) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", "application/json");
        httpHeaders.set("fiware-service", "ANASTACIA");
        httpHeaders.set("fiware-servicepat", "/pilot1");
        httpHeaders.set("Accept", "application/json");


        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(subscriptionDto);
//            System.out.println("JSON = " + json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        HttpEntity<String> httpEntity = new HttpEntity <String> (json.toString(), httpHeaders);

        RestTemplate restTemplate = new RestTemplate();
        SubscriptionResponseDto response = restTemplate.postForObject(url, httpEntity, SubscriptionResponseDto.class);


        return response;
    }
}
