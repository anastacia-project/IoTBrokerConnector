package eu.ubitech.anastacia.IoTBrokerConnector.model;

import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.EntityDto;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.NotifyConditionDto;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.SubscriptionDto;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Chris Paraskeva <ch.paraskeva@gmail.com>
 */

@Document(collection = "subscription")
public class Subscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private String id;

    @Column(nullable = false)
    List<Entity> entities;

    @Column(nullable = false)
    List<String> attributes;

    @Column(nullable = false)
    String reference;

    @Column(nullable = false)
    String requestedDuration;

    @Column(nullable = false)
    List<NotifyCondition> notifyConditions;

    @Column(nullable = false)
    private String throttling;

    @Column(unique = true, nullable = false)
    private String subscriptionId;

    @Column(name = "duration", nullable = false)
    private Long duration;

    @Column(nullable = false, name = "subscriptionStart")
    private Date subscriptionStart = new Date();

    @Column(nullable = false, name = "state")
    private Boolean state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRequestedDuration() {
        return requestedDuration;
    }

    public void setRequestedDuration(String requestedDuration) {
        this.requestedDuration = requestedDuration;
    }

    public List<NotifyCondition> getNotifyConditions() {
        return notifyConditions;
    }

    public void setNotifyConditions(List<NotifyCondition> notifyConditions) {
        this.notifyConditions = notifyConditions;
    }

    public String getThrottling() {
        return throttling;
    }

    public void setThrottling(String throttling) {
        this.throttling = throttling;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Date getSubscriptionStart() {
        return subscriptionStart;
    }

    public void setSubscriptionStart(Date subscriptionStart) {
        this.subscriptionStart = subscriptionStart;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Long parseISO8601 (String input ) {
        int years = 0;
        int months = 0;
        int weeks = 0;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;

        Long duration = Long.valueOf(0);

        Pattern pattern = Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)Y)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)W)?(?:([-+]?[0-9]+)D)?(?:T)?(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)S)?", Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(input);
        if(m.find()){
            if(m.group(2)!=null){
                years = Integer.parseInt(m.group(2));
            }
            if(m.group(3)!=null){
                months = Integer.parseInt(m.group(3));
            }
            if(m.group(4)!=null){
                weeks = Integer.parseInt(m.group(4));
            }
            if(m.group(5)!=null){
                days = Integer.parseInt(m.group(5));
            }
            if(m.group(6)!=null){
                hours = Integer.parseInt(m.group(6));
            }
            if(m.group(7)!=null){
                minutes = Integer.parseInt(m.group(7));
            }
            if(m.group(8)!=null){
                seconds = Integer.parseInt(m.group(8));
            }
            duration = Long.valueOf(seconds +  (minutes + (hours + (days +  weeks*7 + months*30 + years*365)*24)*60 )*60);
        }else{
            System.out.println("Wrong duration ISO-8601 format!");
        }
        return duration;
    }

    public Subscription subscriptionDtoParse(SubscriptionDto subscriptionDto){
        this.setAttributes(subscriptionDto.getAttributes());
        this.setReference(subscriptionDto.getReference());
        this.setRequestedDuration(subscriptionDto.getDuration());
        this.setThrottling(subscriptionDto.getThrottling());

        List<EntityDto> entityDtos = subscriptionDto.getEntities();
        List<Entity> entities = new ArrayList<>();

        for(EntityDto dto : entityDtos){
            Entity entity = new Entity();
            entity.setId(dto.getId());
            entity.setIsPattern(dto.getIsPattern());
            entity.setType(dto.getType());
            entities.add(entity);
        }
        this.setEntities(entities);

        List<NotifyConditionDto> notifyConditionDtos = subscriptionDto.getNotifyConditions();
        List<NotifyCondition> notifyConditions = new ArrayList<>();
        for(NotifyConditionDto dto : notifyConditionDtos){
            NotifyCondition notifyCondition = new NotifyCondition();
            notifyCondition.setCondValues(dto.getCondValues());
            notifyCondition.setType(dto.getType());

            notifyConditions.add(notifyCondition);
        }

        this.setNotifyConditions(notifyConditions);
        return this;
    }
}



