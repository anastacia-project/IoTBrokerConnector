package eu.ubitech.anastacia.IoTBrokerConnector.Utils;

//import util.properties packages
import java.util.Properties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaUtil {
    private final static String TOPIC = "IoTBrokerTopic";
    private final static String BOOTSTRAP_SERVERS = "212.101.173.57:9092";

    public static void produceIoTBrokerTopic(Long key, String data){

        //Assign topicName to string variable
        String topicName = TOPIC;

        // create instance for properties to access producer configs
        Properties props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);

        //Set acknowledgements for producer requests.
        props.put("acks", "all");

        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        //Specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

//        props.put("key.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");
//        props.put("value.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");

        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        System.out.println("WHY!!!");
        producer.send(new ProducerRecord<String, String>(topicName, Long.toString(key), data));
        System.out.println("Message sent successfully");
        producer.close();

    }


}
