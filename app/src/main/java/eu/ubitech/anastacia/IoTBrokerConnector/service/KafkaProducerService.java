package eu.ubitech.anastacia.IoTBrokerConnector.service;

import org.springframework.stereotype.Service;

@Service
public interface KafkaProducerService<K, D> {

//    S register(S s) throws InvalidThrottlingException, InvalidDurationException, AnySubscriptionIDException;

    void produceIoTBrokerTopic(K key, D data);
}
