package eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.ubitech.anastacia.IoTBrokerConnector.model.Subscription;

import java.util.regex.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionResponseDto {

    @JsonProperty(value = "subscribeResponse")
    private SubscribeResponse subscribeResponse;

    public SubscribeResponse getSubscribeResponse() {
        return subscribeResponse;
    }

    public void setSubscribeResponse(SubscribeResponse subscribeResponse) {
        this.subscribeResponse = subscribeResponse;
    }

    private Subscription toSubscription(){
        Subscription subscription = new Subscription();

        subscription.setDuration(parseISO8601(subscribeResponse.getDuration()));
        subscription.setSubscriptionId(subscribeResponse.getSubscriptionId());
        subscription.setThrottling(subscribeResponse.getThrottling());
        return subscription;
    }

    public static Long parseISO8601 (String input ) {
        int years = 0;
        int months = 0;
        int weeks = 0;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;

        Long duration = Long.valueOf(0);

        Pattern pattern = Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)Y)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)W)?(?:([-+]?[0-9]+)D)?(?:T)?(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)S)?", Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(input);
        if(m.find()){
            if(m.group(2)!=null){
                years = Integer.parseInt(m.group(2));
            }
            if(m.group(3)!=null){
                months = Integer.parseInt(m.group(3));
            }
            if(m.group(4)!=null){
                weeks = Integer.parseInt(m.group(4));
            }
            if(m.group(5)!=null){
                days = Integer.parseInt(m.group(5));
            }
            if(m.group(6)!=null){
                hours = Integer.parseInt(m.group(6));
            }
            if(m.group(7)!=null){
                minutes = Integer.parseInt(m.group(7));
            }
            if(m.group(8)!=null){
                seconds = Integer.parseInt(m.group(8));
            }
            duration = Long.valueOf(seconds +  (minutes + (hours + (days +  weeks*7 + months*30 + years*365)*24)*60 )*60);
        }else{
            System.out.println("Wrong duration ISO-8601 format!");
        }
        return duration;
    }
}

