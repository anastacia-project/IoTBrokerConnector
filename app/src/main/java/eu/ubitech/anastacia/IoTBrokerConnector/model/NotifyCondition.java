package eu.ubitech.anastacia.IoTBrokerConnector.model;

import javax.persistence.Column;
import java.util.List;

public class NotifyCondition {

    @Column(nullable = false)
    String type;

    @Column(nullable = false)
    List<String> condValues;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getCondValues() {
        return condValues;
    }

    public void setCondValues(List<String> condValues) {
        this.condValues = condValues;
    }


}
