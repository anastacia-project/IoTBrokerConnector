package eu.ubitech.anastacia.IoTBrokerConnector.service.impl;

import eu.ubitech.anastacia.IoTBrokerConnector.service.KafkaProducerService;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class KafkaProducerServiceImpl implements KafkaProducerService<Long, String> {

    private String topic = System.getenv("KAFKA_TOPIC");
    private String bootstrap = System.getenv("KAFKA_URL");

    private Properties props;
    Producer<String, String> producer;

    public KafkaProducerServiceImpl(){
        // create instance for properties to access producer configs
        props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", bootstrap);

        //Set acknowledgements for producer requests.
        props.put("acks", "all");

        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        //Specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

//        props.put("key.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");
//        props.put("value.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");

        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<String, String>(props);
    }

    @Override
    public void produceIoTBrokerTopic(Long key, String data) {
//        System.out.println("WHY!!!");
        producer.send(new ProducerRecord<String, String>(topic, Long.toString(key), data));
//        System.out.println("Message sent successfully");
//        producer.close();
    }
}
