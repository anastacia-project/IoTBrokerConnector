package eu.ubitech.anastacia.IoTBrokerConnector.model;

import javax.persistence.Column;

public class Entity {

    @Column(nullable = false)
    String type;
    @Column(nullable = false)
    String isPattern;
    @Column(nullable = false)
    String id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsPattern() {
        return isPattern;
    }

    public void setIsPattern(String isPattern) {
        this.isPattern = isPattern;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
