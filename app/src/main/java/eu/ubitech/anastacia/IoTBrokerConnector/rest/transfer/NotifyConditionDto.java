package eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotifyConditionDto {

    @JsonProperty(value = "type")
    String type;

    @JsonProperty(value = "condValues")
    List<String > condValues;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getCondValues() {
        return condValues;
    }

    public void setCondValues(List<String> condValues) {
        this.condValues = condValues;
    }
}
