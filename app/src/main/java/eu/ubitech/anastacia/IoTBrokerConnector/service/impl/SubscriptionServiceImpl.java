package eu.ubitech.anastacia.IoTBrokerConnector.service.impl;

import eu.ubitech.anastacia.IoTBrokerConnector.model.Subscription;
import eu.ubitech.anastacia.IoTBrokerConnector.repository.SubscriptionRepository;
import eu.ubitech.anastacia.IoTBrokerConnector.service.SubscriptionService;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.AnySubscriptionIDException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidThrottlingException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidDurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Component
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService<Subscription, String> {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public Subscription register(Subscription subscription){
        Subscription newSubscription= new Subscription();
        newSubscription.setEntities(subscription.getEntities());
        newSubscription.setAttributes(subscription.getAttributes());
        newSubscription.setReference(subscription.getReference());
        newSubscription.setRequestedDuration(subscription.getRequestedDuration());
        newSubscription.setNotifyConditions(subscription.getNotifyConditions());
        newSubscription.setThrottling(subscription.getThrottling());
        newSubscription.setDuration(subscription.parseISO8601(subscription.getRequestedDuration()));
        newSubscription.setSubscriptionStart(new Date());
        newSubscription.setState(false);

        // Store subscription to database

        return subscriptionRepository.save(newSubscription);
    }

    @Override
    public Subscription setSubscriptionResponse(Subscription subscription) throws InvalidThrottlingException, InvalidDurationException, AnySubscriptionIDException {
        if(null == subscription.getSubscriptionId()){
            throw new AnySubscriptionIDException();
        }

        subscription.setSubscriptionId(subscription.getSubscriptionId());
        subscription.setState(true);

        subscriptionRepository.save(subscription);
        return findBySubscriptionId(subscription.getSubscriptionId());
    }

    @Override
    public Subscription findBySubscriptionId(String subscriptionId) {
        return null;
    }



//    @Override
//    public List<Subscription> findAll() {
//        return null;
//    }
//
//    @Override
//    public void updateSubscription(String username, Subscription subscription) {
//
//    }
//
//    @Override
//    public void removeSubscription(String subscriptionId) {
//
//    }
//
//    @Override
//    public void disableSubscription(String subscriptionId) {
//
//    }
}
