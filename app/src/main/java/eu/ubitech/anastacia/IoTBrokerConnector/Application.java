package eu.ubitech.anastacia.IoTBrokerConnector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

@ComponentScan(basePackages = {
        "eu.ubitech.anastacia.IoTBrokerConnector.rest",
        "eu.ubitech.anastacia.IoTBrokerConnector.service"
})

@EnableMongoRepositories("eu.ubitech.anastacia.IoTBrokerConnector.repository")
@EntityScan(basePackages = {"eu.ubitech.anastacia.IoTBrokerConnector.model"})
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class, SolrAutoConfiguration.class})
public class Application {

    private final static Logger LOGGER = LogManager.getLogger(Application.class.getName());
    @Value("${app.settings.timezone}")
    private String timezone;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    public void init() throws ParseException {
        TimeZone.setDefault(TimeZone.getTimeZone(timezone));
        LOGGER.info("Starting Application in timezone " + timezone + " (" + new Date() + ")");
    }

}

