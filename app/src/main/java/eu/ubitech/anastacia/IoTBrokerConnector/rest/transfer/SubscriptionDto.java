package eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionDto {

    @JsonProperty(value = "entities")
    List<EntityDto> entities;
    @JsonProperty(value = "attributes")
    List<String> attributes;
    @JsonProperty(value = "reference")
    String reference;
    @JsonProperty(value = "duration")
    String duration;
    @JsonProperty(value = "notifyConditions")
    List<NotifyConditionDto> notifyConditions;
    @JsonProperty(value = "throttling")
    String throttling;

    public List<EntityDto> getEntities() {
        return entities;
    }

    public void setEntities(List<EntityDto> entities) {
        this.entities = entities;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<NotifyConditionDto> getNotifyConditions() {
        return notifyConditions;
    }

    public void setNotifyConditions(List<NotifyConditionDto> notifyConditions) {
        this.notifyConditions = notifyConditions;
    }

    public String getThrottling() {
        return throttling;
    }

    public void setThrottling(String throttling) {
        this.throttling = throttling;
    }
}
