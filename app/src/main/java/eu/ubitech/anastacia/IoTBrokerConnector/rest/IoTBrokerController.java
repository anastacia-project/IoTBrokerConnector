package eu.ubitech.anastacia.IoTBrokerConnector.rest;


import eu.ubitech.anastacia.IoTBrokerConnector.Utils.IoTBrokerUtil;
import eu.ubitech.anastacia.IoTBrokerConnector.model.Subscription;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.SubscriptionDto;
import eu.ubitech.anastacia.IoTBrokerConnector.rest.transfer.SubscriptionResponseDto;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.AnySubscriptionIDException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidDurationException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.exception.InvalidThrottlingException;
import eu.ubitech.anastacia.IoTBrokerConnector.service.impl.KafkaProducerServiceImpl;
import eu.ubitech.anastacia.IoTBrokerConnector.service.impl.SubscriptionServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

@RestController
public class IoTBrokerController {
    private String ac;
    private Long t0;
    private List<Long> times;
    private Long iterator = Long.valueOf(0);

    @Value("${app.settings.timezone}")
    private String timezone;
    private final static Logger LOGGER = LogManager.getLogger(IoTBrokerController.class.getName());

    @Autowired
    SubscriptionServiceImpl subscriptionService;

    @Autowired
    KafkaProducerServiceImpl kafkaProducerService;

    public IoTBrokerController(){
        ac = "";
        t0 = Long.valueOf(0);
        times = new ArrayList<>();
    }

    @RequestMapping(path = "/noresponse", method = RequestMethod.POST)
    public HttpEntity noresponse() throws InterruptedException {
        Thread.sleep(10000);
        String data = "";
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/noresponse/updateContext", method = RequestMethod.POST)
    public HttpEntity unoresponse() throws InterruptedException {
        Thread.sleep(10000);
        String data = "";
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/noresponse/queryContext", method = RequestMethod.POST)
    public HttpEntity qnoresponse() throws InterruptedException {
        Thread.sleep(10000);
        String data = "";
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/badresponse/queryContext", method = RequestMethod.POST)
    public HttpEntity bad_response() {
        String data;
        data = "{\"name\":\"ENTITY_NOT_FOUND\",\"message\":\"The entity with the requested id [qa_name_01] was not found.\"}";

        ResponseEntity<String> r = ResponseEntity.status(404).body(data);
        return r;
    }

    @RequestMapping(path = "/v1/updateContext", method = RequestMethod.POST)
    public HttpEntity updateContext(HttpServletRequest request) {
        return this.record(request);
    }

    @RequestMapping(path = "/v1/queryContext", method = RequestMethod.POST)
    public HttpEntity queryContext(HttpServletRequest request) {
        return this.record(request);
    }

    @RequestMapping(path = "/accumulate", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
    public HttpEntity accumulate(HttpServletRequest request) {
        ZonedDateTime now = ZonedDateTime.now( ZoneOffset.UTC );
        System.out.println("Received datetime: " + now.toString());
        return this.record(request);
    }

    public HttpEntity record(HttpServletRequest request){
        String s = "";
        Boolean send_continue = false;


        if (t0 == 0) {
            Date date = new Date();
            t0 = date.getTime();
            times.add(Long.valueOf(0));
        }
        else {
            Date date = new Date();
            Long delta = date.getTime() - t0;
            times.add(delta/1000);
        }

        System.out.println(request.toString());
        s += request.getMethod() + " " + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getServletPath();


        String params = "";
        Map paramMap = request.getParameterMap();
        Set paramKeys = paramMap .keySet();

        // Check for query params
        for(Object key : paramKeys){
            if(params.equals("")){
                params = key.toString() + "=" + paramMap.get(key);
            }else{
                params += "&" + key + "=" + paramMap.get(key);
            }
        }

        if(params.equals("")){
            s += "\n";
        }else{
            s += "?" + params + "\n";
        }

        // Store headers
        Enumeration<String> headerNames = request.getHeaderNames();

        while(true){
            String name = headerNames.nextElement();
            if(name!=null){
                s += name + ": " + request.getHeader(name) + "\n";
                if(name.equals("Expect") && (request.getHeader(name).equals("100-continue"))){
                    send_continue = true;
                }
            }else{
                break;
            }
        }

        // Store payload

        BufferedReader bodyReader = null;
        String body = "";
        try {
            bodyReader = request.getReader();
            while(true){
                String line = bodyReader.readLine();
                if(line!=null && line.length()!=0){
                    body += "\n" + line ;
                }else{
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!body.equals("")){
            s +=  body;
            kafkaProducerService.produceIoTBrokerTopic(iterator++, body);
        }
        s += "\n=======================================\n";
        ac += s;




        System.out.println(s);
        ResponseEntity r;
        if(send_continue){
            r = ResponseEntity.status(100).body("");

        }else{
            r = ResponseEntity.status(200).body("");
        }



        return r;
    }

    @RequestMapping(path = "/dump", method = RequestMethod.GET)
    public HttpEntity dump() {
//        return global ac
        ResponseEntity<String> r = ResponseEntity.status(200).body(ac);
        return r;
    }

    @RequestMapping(path = "/times", method = RequestMethod.GET)
    public HttpEntity times() {
//        return ', '.join(map(str,times)) + '\n'

        String data = "";
        Iterator iterator = times.iterator();

        while(iterator.hasNext()){
            Long time = (Long) iterator.next();
            if(iterator.hasNext()){
                data += time.toString() + ", ";
            }else{
                data += time.toString() + "\n";
            }
        }

        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/number", method = RequestMethod.GET)
    public HttpEntity number()  {
//        return str(len(times)) + '\n'
        Integer size = times.size();
        String data = size.toString();
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/reset", method = RequestMethod.POST)
    public HttpEntity reset()  {
//        global ac, t0, times
//        ac = ''
//        t0 = ''
//        times = []

        String data = "";
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/pid", method = RequestMethod.GET)
    public HttpEntity getPid()  {
//        return str(os.getpid())
        String data = "";
        ResponseEntity<String> r = ResponseEntity.status(200).body(data);
        return r;
    }

    @RequestMapping(path = "/subscribe", method = RequestMethod.POST)
    public HttpEntity subscribe(@RequestBody SubscriptionDto subscriptionDto) throws InvalidDurationException, InvalidThrottlingException, AnySubscriptionIDException {

        Subscription subscription = new Subscription();

        subscription.subscriptionDtoParse(subscriptionDto);
        Subscription currentSub = subscriptionService.register(subscription);

        String url = "http://155.54.210.142:1026/v1/subscribeContext";
        SubscriptionResponseDto response = IoTBrokerUtil.sendSubscribe(url, subscriptionDto);

        if(response!=null) {
            String subId = response.getSubscribeResponse().getSubscriptionId();
            currentSub.setSubscriptionId(subId);
            subscriptionService.setSubscriptionResponse(currentSub);
        }else{
            LOGGER.warn("Problem with the subscription" + timezone + " (" + new Date() + ")");
        }

        ResponseEntity<String> r = ResponseEntity.status(200).body("");
        return  new ResponseEntity<>( HttpStatus.ACCEPTED);
    }


}
